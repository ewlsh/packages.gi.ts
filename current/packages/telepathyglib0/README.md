# TelepathyGLib 0.12

TypeScript definitions for TelepathyGLib. Generated from version 0.12.

Generated with [gi.ts](https://gitlab.gnome.org/ewlsh/gi.ts) and tracked in [packages.gi.ts](https://gitlab.gnome.org/ewlsh/packages.gi.ts).
