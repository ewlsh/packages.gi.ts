# GIRepository 2.0

TypeScript definitions for GIRepository. Generated from version 1.66.1.

Generated with [gi.ts](https://gitlab.gnome.org/ewlsh/gi.ts) and tracked in [packages.gi.ts](https://gitlab.gnome.org/ewlsh/packages.gi.ts).
