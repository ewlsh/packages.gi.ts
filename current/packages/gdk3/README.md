# Gdk 3.0

TypeScript definitions for Gdk. Generated from version 3.24.24.

Generated with [gi.ts](https://gitlab.gnome.org/ewlsh/gi.ts) and tracked in [packages.gi.ts](https://gitlab.gnome.org/ewlsh/packages.gi.ts).
