# WebKit2 4.0

TypeScript definitions for WebKit2. Generated from version 2.30.4.

Generated with [gi.ts](https://gitlab.gnome.org/ewlsh/gi.ts) and tracked in [packages.gi.ts](https://gitlab.gnome.org/ewlsh/packages.gi.ts).
