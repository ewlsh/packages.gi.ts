# Rsvg 2.0

TypeScript definitions for Rsvg. Generated from version 2.50.2.

Generated with [gi.ts](https://gitlab.gnome.org/ewlsh/gi.ts) and tracked in [packages.gi.ts](https://gitlab.gnome.org/ewlsh/packages.gi.ts).
