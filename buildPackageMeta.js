/// <reference lib="ES2019" />

// @ts-check

const fs = require('fs');
const crypto = require('crypto');
const semver = require('semver');

const versionMap = new Map();

const LICENSE = `Copyright © 2020 Evan Welsh

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
`;

const createREADME = (name, api_version, package_version, npm_version) => {
    return `# ${name} ${api_version}

TypeScript definitions for ${name}. Generated from version ${package_version}.

Generated with [gi.ts](https://gitlab.gnome.org/ewlsh/gi.ts) and tracked in [packages.gi.ts](https://gitlab.gnome.org/ewlsh/packages.gi.ts).
`
};

function latestVersion(name, majorVersion) {
    const versions = versionMap.get(name);

    if (!versions) {
        console.error(`No versions for ${name}`);

        return null;
    }

    return versions[majorVersion];
}

function packageVersion(meta, revision) {
    const rawVersion = meta.package_version || meta.api_version;
    const sem = semver.parse(rawVersion) || semver.coerce(rawVersion);

    if (!sem) {
        throw new Error(`Invalid raw version: ${rawVersion} for ${meta.name}`);
    }

    sem.patch = revision;

    if (sem.minor >= 90) {
        return `${sem.format()}-prerelease`;
    }

    return `${sem.format()}`;
}

const prefix = '@gi-types';

/**
 * @param {*} meta 
 * @param {string} version 
 * @param {string} hash 
 * @param {string} gitHead
 * @param {string} tag 
 * @param {boolean} [hasOverride]
 * @param {boolean} [private] 
 */
function buildPackageJSON(meta, version, hash, gitHead, tag, hasOverride = false, private = false) {
    return {
        "name": `${prefix}/${meta.name.toLowerCase()}`,
        ...(private ? { "private": true } : {
            "publishConfig": {
                "access": "public",
                "tag": tag
            }
        }),
        "version": `${version}`,
        "description": `TypeScript definitions for ${meta.name}`,
        "license": "MIT",
        "contributors": [
            {
                "name": "Evan Welsh",
                "url": "https://gitlab.gnome.org/ewlsh/",
                "githubUsername": "ewlsh"
            }
        ],
        "main": "",
        "files": [
            "index.d.ts",
            ...(hasOverride ? ["override.d.ts"] : []),
            "doc.json",
            "package.json",
            ...(private ? [] : ["README.md", "LICENSE"]),
        ],
        "types": hasOverride ? "override.d.ts" : "index.d.ts",
        "repository": {
            "type": "git",
            "url": "https://gitlab.gnome.org/ewlsh/packages.gi.ts.git",
            "directory": `packages/${meta.slug}`
        },
        "scripts": {},
        "contentHash": hash,
        ...(meta.imports && Object.keys(meta.imports).length > 0 ? {
            "dependencies":
                Object.fromEntries(Object.entries(meta.imports).map(([im, api_version]) => {
                    const version = latestVersion(im, api_version);

                    if (version)
                        return [`${prefix}/${im.toLowerCase()}`, `^${version}`];
                    else
                        return null;
                }).filter(a => a != null).sort(([a], [b]) => a.localeCompare(b)))
        } : {}),
        "typeScriptVersion": "4.1",
        "gitHead": gitHead
    };
}

/**
 * @param {string} dir 
 */
function getDirectories(dir) {
    return fs.readdirSync(dir, {
        withFileTypes: true
    })
        .filter(child => child.isDirectory())
        .map(child => child.name);
}

/**
 * @param {string} file 
 */
function fileHash(file) {
    return crypto.createHash('md5').update(file).digest('hex');
}

/**
 * @param {string} directory 
 * @param {string[] | null} [includeList] 
 */
function generatePackages(directory, includeList = null) {
    console.log(`Generating packages for ${directory}.`);

    const packages = getDirectories(directory);

    return packages.map(package => {
        const meta = JSON.parse(fs.readFileSync(`${directory}/${package}/doc.json`, {
            encoding: 'utf-8'
        }));

        const typeFile = fs.readFileSync(`${directory}/${package}/index.d.ts`, {
            encoding: 'utf-8'
        });

        const overrideExists = fs.existsSync(`${directory}/${package}/override.d.ts`);

        let generatedSourceHash = fileHash(typeFile);

        let currentPackageVersion = null;
        let currentSourceHash = null;
        let gitHead = null;

        let version = packageVersion(meta, 0);

        try {
            let packagejson = JSON.parse(fs.readFileSync(`${directory}/${package}/package.json`, {
                encoding: 'utf-8'
            }));

            currentSourceHash = packagejson["contentHash"];
            currentPackageVersion = packagejson["version"];
            gitHead = packagejson["gitHead"];

            if (currentPackageVersion) {
                const sem = semver.parse(currentPackageVersion);
                version = packageVersion(meta, sem.patch + 1);
            }
        } catch (err) { }

        if (currentPackageVersion && currentSourceHash === generatedSourceHash) {
            version = currentPackageVersion;
        }

        meta.slug = package;

        const isPrivate = includeList && !includeList.includes(meta.name);

        if (!isPrivate) {
            const versions = versionMap.get(meta.name) || {};

            versions[meta.api_version] = version;

            versionMap.set(meta.name, versions);
        }

        return { isPrivate, directory, package, hash: generatedSourceHash, overrideExists, gitHead, version, meta };
    });
}

/**
 * @param {ReturnType<typeof generatePackages>} packages 
 * @param {string} tag 
 */
function printPackages(packages, tag) {
    packages.forEach(({ isPrivate, directory, package, hash, overrideExists, gitHead, version, meta }) => {
        if (!isPrivate) {
            const json = buildPackageJSON(
                meta,
                version,
                hash,
                gitHead,
                tag,
                overrideExists,
                isPrivate
            );

            const README = createREADME(meta.name, meta.api_version, meta.package_version, version);

            fs.writeFileSync(`${directory}/${package}/package.json`, `${JSON.stringify(json, null, 4)}\n`);


            fs.writeFileSync(`${directory}/${package}/LICENSE`, `${LICENSE}`);
            fs.writeFileSync(`${directory}/${package}/README.md`, `${README}`);
        }
    });
}



const latest = generatePackages("./current/packages");
const next = generatePackages("./next/packages", [
    "Gst",
    "Gdk",
    "Graphene",
    "Gtk",
    "Gsk"
]);
const latestShell = generatePackages("./shell/packages", [
    "Cally",
    "Cogl",
    "CoglPango",
    "Clutter",
    "ClutterX11",
    "Meta",
    "Shell",
    "Shew",
    "Gvc",
    "St"
]);

printPackages(latest, "latest");
printPackages(next, "next");
printPackages(latestShell, "latest");

console.log(versionMap);